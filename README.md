### django donate

(C) 2013 Chris Young

TODO chris!

## Instalation:

This is designed to work as a django APP - as part of a bigger django Project.
Thus you can put this dir into a django project folder, and expect it to work as normal.

There is also a basic django Project available for you to use, if you want it stand alone.

To install this basic project:

Go into the parent directory of this git repo. (out one level from this README.md file)

Assuming that this folder is called donate, type:

    ./donate/setup.sh

if it isn't called donate, then the script will rename this folder to be called that,
and set up the parent directory as a django project, along with a virtualenv, and all needed
pip installs.
