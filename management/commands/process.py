from django.core.management.base import BaseCommand, CommandError
from donate.models import Donor, Donation, Commitment
from donate.gateway import GatewayConnector
import datetime
import django.utils

class Command(BaseCommand):
    args = ''
    help = 'Processes periodic payments'
    
    def handle(self, *args, **options):
        try:
            commitments = Commitment.objects.filter(next_payment_date__lte=datetime.date.today())
        except Commitment.DoesNotExist:
            self.stdout.write('Nothing to process\n')
            return
        
        for c in commitments:
            try:
                self.process(c.id)
            except c.DoesNotExist:
                raise CommandError('Does not exist')
            
            self.stdout.write('Done.')
            
    def process(self, commitment_id):
        
        """ Set up objects """
        commitment = Commitment.objects.get(pk=commitment_id)
        donation = Donation()
        donation.commitment = commitment
        donation.donor = commitment.donor
        donation.date_time = django.utils.timezone.now()
        donation.test = 1
        commitment.iterations_passed += 1
        donation.iteration = commitment.iterations_passed
        
        """ Copy some purposely redundant data on the donation """
        donation.merge(commitment,allowedkeys=['currency','amount','cc_last_four','cc_expiry','project_code','project_name','client_id'])
        donation.merge(commitment.donor,allowedkeys=['title','f_name','l_name','address','address2','city','state','postcode','country','phone','email'])
        
        """ Process the payment """
        gateway = GatewayConnector()
        gateway.transaction = {'amount': commitment.amount,
                              'currency': commitment.currency,
                              'currency-decimals': 2,
                              'client_id': commitment.client_id
                              }
        gateway.generate_xml_request('trigger')
        gateway.send()
        
        """ Save transaction data to the donation table """
        gr = gateway.response_data
        donation.merge({
                        'request_type': gr.requesttype.text,
                        'action_type': gr.actiontype.text,
                        'response_code': gr.responsecode.text,
                        'response_text': gr.responsetext.text,
                        'transaction_id': gr.txnid.text,
                        'settlement_date': gr.settlementdate.text,
                        'message_id': gr.messageid.text,
                        'xml_request': gateway.xml_request,
                        'xml_response': gateway.xml_response
                        })
        donation.save()
        commitment.save()