from django.template.loader import render_to_string
import urllib
import urllib2
from django.utils import timezone
import time
from xml.dom.minidom import parseString
from bs4 import BeautifulSoup
import ConfigParser
from django.conf import settings

class GatewayConnector():
    xml_request = ""
    xml_response = ""
    gateway_url = ""
    gateway_triggered_url = ""
    transaction = {}
    response_data = {}
    
    def __unicode__(self):
        return self.xml_request
    
    def generate_xml_request(self,type):
        # Load gateway config file
        config = ConfigParser.RawConfigParser()
        config.read(settings.SITE_ROOT+'/donate/config/gateways.ini')
        gateway_name = config.get('basic','gateway')
        self.config = dict(config.items(gateway_name))        
        
        # Set URL to post to
        if (type=='payment'): self.gateway_url = config.get(gateway_name,'url_payment')
        else: self.gateway_url = config.get(gateway_name,'url_periodic')
        # Set defaults
        self.transaction['timestamp'] = time.strftime('%Y%m%d%H%M%S')+"000000"
        self.transaction['message_id'] = 'asdasdasda';
                
        # The gateway might require the amount as an integer rather than a float - so multiply by 10 * number_of_decimals
        if (config.get(gateway_name,'amount_datatype')=='int'):
            field_values_config = ConfigParser.RawConfigParser()
            field_values_config.read(settings.SITE_ROOT+'/donate/config/fieldvalues.ini')
            self.transaction['currency-decimals'] = field_values_config.get('currency-decimals',self.transaction['currency']) 
            self.transaction['cents'] = (int(self.transaction['amount']) * pow(10,int(self.transaction['currency-decimals'])))
             
        # Load and render the xml template
        if (type in config.get('basic','request_types')): 
            self.xml_request = render_to_string('donate/gateways/'+gateway_name+'/'+type+'.xml', {'transaction':self.transaction,'config':self.config})
            
    def send(self):
        response = urllib2.urlopen(self.gateway_url,self.xml_request)
        self.xml_response = response.read()
        # Make the XML response into an object for manipulation
        self.response_data = BeautifulSoup(self.xml_response)
        # Format XML response for readability
        self.xml_response = self.response_data.prettify()
        
            
