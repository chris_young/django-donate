from django.shortcuts import render_to_response, get_object_or_404, render
from django.http import HttpResponseRedirect, HttpResponse, HttpRequest
from django.template import RequestContext
from django.template.loader import render_to_string
from django.core.urlresolvers import reverse
import django.utils
from django import forms
import ConfigParser
from donate.models import Donation, Donor, Commitment, Project, DonateForm, DonorForm, CommitmentForm
from donate.gateway import GatewayConnector
import donate.logic as logic
from django.core.mail import send_mail
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponseRedirect
from models import OrderedModel
from django.db import transaction
from django.contrib.contenttypes.models import ContentType, ContentTypeManager
from django.db import models
from django.contrib.formtools.preview import FormPreview
from django.conf import settings

def home(request):
    return render_to_response('home.html',{'foo':'bar'},context_instance=RequestContext(request))

def about(request):
    return render_to_response('about.html',{'foo':'bar'},context_instance=RequestContext(request))


class DonateFormPreview(FormPreview):
    form_template = "donate/form.html"
    preview_template = "donate/preview.html"

    def done(self, request, cleaned_data): 
        config = ConfigParser.RawConfigParser()
        config.read(settings.SITE_ROOT+'/donate/config/config.ini')
        
        # choose between one-off payment or triggered payment
        if (cleaned_data['repeat']=='none'): 
            type = 'payment' 
        else: 
            type = 'triggered'
        
        # Save the donor first, use their id as a client ID in securepay
        cleaned_data['ip'] = request.META['REMOTE_ADDR']
        donor = logic.saveDonor(cleaned_data)
        client_id = config.get('basic','client_id_prefix')+str(donor.id)
        
        # Send the data to the gateway - get the responses
        response_data, xml_request, xml_response = logic.processTransaction(type,cleaned_data,client_id)
        
        # Save the donation or commitment
        id, message_id = logic.save(type, response_data, cleaned_data, donor, client_id, xml_request, xml_response)
            
        # Redirect to a thankyou page
        if (cleaned_data['repeat']=='none'): 
            return HttpResponseRedirect(reverse('donate.views.thanks_single', kwargs={'pk':id,'message_id':message_id})) # Redirect after POST
        else:
            return HttpResponseRedirect(reverse('donate.views.thanks_regular', kwargs={'pk':id,'message_id':message_id})) # Redirect after POST


def thanks_single(request,pk,message_id):
    donation = get_object_or_404(Donation,pk=pk,message_id=message_id)
    config = ConfigParser.RawConfigParser()
    config.read(settings.SITE_ROOT+'/donate/config/config.ini')
    send_mail('Thank you for your donation',render_to_string('donate/emails/thanks_single.txt',{'donation':donation, 'donor':donation.donor}),config.get('basic','from_email'),[donation.email])
    return render_to_response('donate/thanks_single.html',{'donation':donation,'donor':donation.donor},context_instance=RequestContext(request))
    
def thanks_regular(request,pk,message_id):
    commitment = get_object_or_404(Commitment,pk=pk,message_id=message_id)
    config = ConfigParser.RawConfigParser()
    config.read('donate/config/config.ini')
    send_mail('Thank you for your donation',render_to_string('donate/emails/thanks_regular.txt',{'commitment':commitment, 'donor':commitment.donor}),config.get('basic','from_email'),[donation.email])
    return render_to_response('donate/thanks_regular.html',{'commitment':commitment,'donor':commitment.donor},context_instance=RequestContext(request))

# The below view is used by the admin panel to re-order list items
@staff_member_required
@transaction.commit_on_success
def admin_move_ordered_model(request, direction, model_type_id, model_id):
    if direction == "up":
        OrderedModel.move_up(model_type_id, model_id)
    else:
        OrderedModel.move_down(model_type_id, model_id)
    
    ModelClass = ContentType.objects.get(id=model_type_id).model_class()
    
    app_label = ModelClass._meta.app_label
    model_name = ModelClass.__name__.lower()

    url = "/admin/%s/%s/" % (app_label, model_name)
    
    return HttpResponseRedirect(url)
