from django.conf.urls import patterns, include, url
import views

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', views.home, name="home"),
    url(r'^about$', views.about, name="about"),
    url(r'^donate/', include('donate.urls')),
    #url(r'^contact/', include('contact_form.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/orderedmove/(?P<direction>up|down)/(?P<model_type_id>\d+)/(?P<model_id>\d+)/$', 'donate.views.admin_move_ordered_model', name="admin-move"),
)
