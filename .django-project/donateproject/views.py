from django.template import RequestContext
from django.shortcuts import render_to_response

def home(request):
    return render_to_response('home.html',{"foo":"bar"},context_instance=RequestContext(request))

def about(request):
    return render_to_response('about.html',{"foo":"bar"},context_instance=RequestContext(request))
