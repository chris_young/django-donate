from django.conf.urls import patterns, include, url
from donate.models import DonateForm
from donate.views import DonateFormPreview

urlpatterns = patterns('donate.views',
    url(r'^$',DonateFormPreview(DonateForm),name="donate"),
    url(r'^thanks_single/(?P<pk>\d+)-(?P<message_id>\w+)','thanks_single'),
    url(r'^thanks_regular/(?P<pk>\d+)-(?P<message_id>\w+)','thanks_regular'),
)
