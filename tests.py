"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from donate.gateway import GatewayConnector
from donate import logic
from donate.models import Project
from django.utils import timezone

class SimpleTest(TestCase):
    type='triggered'
    client_id = 'csytest124236'
    project = Project()
    project.code = 'TEST'
    project.name = "Test"
    form_data = {'title': 'Mr',
                 'f_name': 'test',
                 'l_name': "d'tester",
                 'address': '1 test st',
                 'address2': 'test',
                 'city': 'test city',
                 'state': 'test state',
                 'postcode': '1DB 4DS',
                 'country': 'United Kingdom',
                 'phone': '07983833933',
                 'email': 'test@test.com',
                 'allow_marketing': False,
                 'ip': "10.0.0.1",
                 'currency': 'USD',
                 'amount': 10.0,
                 'start_date': '30/04/2012',
                 'repeat': 'monthly',
                 'cc_number': 4444333322221111,
                 'cc_expiry': timezone.now(),
                 'cc_cvv': 433,
                 'project': project,
                 }
    
    def test_save_donor(self):
        self.donor = logic.saveDonor(self.form_data)
        self.client_id = 'csytest'+str(self.donor.id)
        self.assertGreater(self.donor.id, 0, 'donor > 0')
        
    def test_xml_gateway(self):
        self.response_data, self.xml_request, self.xml_response = logic.processTransaction(self.type,self.form_data,self.client_id)
        self.assertEqual('Successful',self.response_data.responsetext.text)
        
        