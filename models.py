from django.db import models
from django import forms
import ConfigParser
import datetime
from django.forms.extras.widgets import SelectDateWidget
from donate.widgets import MonthYearWidget
from donate.meta_models import OrderedModel
from django.conf import settings

class Project(OrderedModel):
    code = models.CharField(max_length=4)
    name = models.CharField(max_length=200)
    def __unicode__(self):
        return self.code
    
class Donor(models.Model):
    created_date = models.DateTimeField('Date Created',auto_now_add=True)
    title = models.CharField(max_length=4)
    f_name = models.CharField(max_length=200)
    l_name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    address2 = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    state = models.CharField(max_length=200)
    postcode = models.CharField(max_length=10)
    country = models.CharField(max_length=200)
    phone = models.CharField(max_length=200)
    email = models.EmailField(max_length=200)
    ip = models.IPAddressField()
    allow_marketing = models.BooleanField()
    exported = models.BooleanField()
    def __unicode__(self):
        return self.f_name+" "+self.l_name
    
class Commitment(models.Model): 
    donor = models.ForeignKey(Donor)
    created_date = models.DateTimeField('Date Created',auto_now_add=True)
    amount = models.FloatField()
    currency = models.CharField(max_length=3)
    start_date = models.DateField()
    frequency = models.CharField(max_length=10)
    next_payment_date = models.DateField()
    iterations_total = models.IntegerField()
    iterations_passed = models.IntegerField(default=0)
    type = models.CharField(max_length=25)
    result = models.CharField(max_length=1)
    test = models.BooleanField()
    cc_last_four = models.CharField(max_length=100)
    cc_expiry = models.DateField()
    project_code = models.CharField(max_length=4)
    project_name = models.CharField(max_length=100)
    ip = models.IPAddressField()
    request_type = models.CharField(max_length=25)
    action_type = models.CharField(max_length=25)
    periodic_type = models.IntegerField()
    client_id = models.CharField(max_length=25)
    response_code = models.IntegerField()
    response_text = models.CharField(max_length=25)
    message_id = models.CharField(max_length=25)
    cancelled = models.BooleanField(default=False)
    cancel_date = models.DateTimeField(null=True)
    exported = models.BooleanField(default=False)
    xml_request = models.TextField()
    xml_response = models.TextField()
    def __unicode__(self):
        return self.message_id
    
class Donation(models.Model):
    commitment = models.ForeignKey(Commitment,null=True)
    donor = models.ForeignKey(Donor)
    date_time = models.DateTimeField(auto_now_add=True)
    amount = models.FloatField()
    currency = models.CharField(max_length=3)
    project_code = models.CharField(max_length=5)
    project_name = models.CharField(max_length=255)
    iteration = models.IntegerField(default=1)
    cc_last_four = models.CharField(max_length=100)
    cc_expiry = models.DateField()
    request_type = models.CharField(max_length=25)
    action_type = models.CharField(max_length=25,null=True)
    response_code = models.IntegerField()
    response_text = models.CharField(max_length=255)
    xml_request = models.TextField()
    xml_response = models.TextField() 
    transaction_id = models.IntegerField()
    client_id = models.CharField(max_length=255)
    purchase_order = models.CharField(max_length=255)
    message_id = models.CharField(max_length=255)
    test = models.BooleanField()
    settlement_date = models.IntegerField()
    title = models.CharField(max_length=4)
    f_name = models.CharField(max_length=200)
    l_name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    address2 = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    state = models.CharField(max_length=200)
    postcode = models.CharField(max_length=10)
    country = models.CharField(max_length=200)
    phone = models.CharField(max_length=200)
    email = models.EmailField(max_length=200)
    exported = models.BooleanField(default=False)
    def __unicode__(self):
        return self.message_id
    
    # this function is used when merging data from a different object
    def merge(self,theobject,allowedkeys=False):
        if isinstance(theobject,dict):
            d = theobject
        else:
            d = theobject.__dict__
            
        for key in d:
            if key in self.__dict__ and key[0]!='_' and key != 'id' and \
                    (not allowedkeys or key in allowedkeys):
                 setattr(self,key,d[key])

# This object is used for saving the submited data
class DonateForm(forms.Form):
    field_values_config = ConfigParser.RawConfigParser()
    field_values_config.read('/home/chris/gbadonate/donate/config/fieldvalues.ini')
    countries = field_values_config.items('countries')
    currencies = field_values_config.items('currencies')
    repeats = field_values_config.items('repeat')
    card_types = field_values_config.items('card_types')
    title = forms.TypedChoiceField(choices=(('Mr','Mr'),('Ms','Ms'),('Miss','Miss'),('Mrs','Mrs'),('Dr','Dr')))
    f_name = forms.CharField(max_length=200)
    l_name = forms.CharField(max_length=200)
    address = forms.CharField(max_length=200)
    address2 = forms.CharField(max_length=200)
    city = forms.CharField(max_length=200)
    state = forms.CharField(max_length=200)
    postcode = forms.CharField(max_length=10)
    country = forms.ChoiceField(choices=countries)
    phone = forms.CharField(max_length=200)
    email = forms.EmailField(max_length=200)
    allow_marketing = forms.BooleanField(required=False, initial=True)
    amount = forms.FloatField()
    currency = forms.ChoiceField(choices=currencies)
    project = forms.ModelChoiceField(Project.objects.all())
    repeat = forms.ChoiceField(required=True,choices=repeats,widget=forms.RadioSelect)
    months = forms.ChoiceField(initial=36,choices=((12,12),(24,24),(36,36)))
    quarters = forms.ChoiceField(initial=12,label='times',choices=((4,4),(8,8),(12,12)))
    years = forms.ChoiceField(initial=3,choices=((3,3),(4,4),(5,5)))
    start_date = forms.DateField(widget=SelectDateWidget,initial=datetime.date.today())
    cc_name = forms.ChoiceField(choices=card_types,label="Card type")
    cc_number = forms.CharField(max_length=100, label="Card Number")
    cc_cvv = forms.CharField(max_length=100, label="CVV security code")
    cc_expiry = forms.DateField(label="Card Expire date", widget=MonthYearWidget)

# This object model is used for saving the submited data
class DonorForm(forms.ModelForm):
    class Meta:
        model = Donor
        fields = ('title','f_name','l_name','address','address2','city','state','postcode','country','phone','email','allow_marketing','ip')
        
# This object model is used for saving the submited data
class CommitmentForm(forms.ModelForm):
    class Meta:
        model = Commitment
        fields = ('donor','currency','start_date','next_payment_date','frequency','iterations_total','type','result','test','cc_last_four','cc_expiry','project_code','project_name', 'amount','ip',
                  'request_type','action_type','periodic_type','client_id','response_code','response_text','message_id','cancelled','exported','xml_request','xml_response')        

