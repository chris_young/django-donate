First Name: {{ donor.f_name }}
Last Name: {{ donor.l_name }}
Address: {{ donor.address }}
 {{ donor.address2 }}
City: {{ donor.city }}
State: {{ donor.state }}
Postcode: {{ donor.postcode }}
Country: {{ donor.country }}
Phone: {{ donor.phone }}
Email: {{ donor.email }}
I {% if donor.allowmarketing %}{% else %}do not {% endif %}want to receive promotional material