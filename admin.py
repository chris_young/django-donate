from django.contrib import admin
from donate.models import Project, Donor, Commitment, Donation
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

class ProjectAdmin(admin.ModelAdmin):
    list_display = ('code','name','order_link')
    
class CommitmentInline(admin.TabularInline):
    model = Commitment
    extra = 0
    fields = ('message_id','donor','currency','amount','frequency','iterations_total','iterations_passed','next_payment_date','test','exported')
    readonly_fields = fields
    can_delete = False
    
class DonationInline(admin.TabularInline):
    model = Donation
    extra = 0
    fields = ('message_id','donor','currency','amount','test','exported','commitment')
    readonly_fields = fields
    can_delete = False
    
class DonorAdmin(admin.ModelAdmin):
    list_display = ('f_name','l_name','country','postcode','email','exported')
    list_display_links = ('f_name','l_name')
    list_filter = ['created_date','country','exported']
    search_fields = ['f_name','l_name','postcode','email']
    date_hierarchy = 'created_date'
    readonly_fields = ['exported','ip']
    inlines = [CommitmentInline,DonationInline]

class CommitmentAdmin(admin.ModelAdmin):
    list_display = ('message_id','donor','currency','amount','frequency','iterations_total','iterations_passed','next_payment_date','test','exported')
    readonly_fields = ['donor','start_date','type','result','test','cc_last_four','ip','xml_request','xml_response',
                       'request_type','action_type','periodic_type','response_code','client_id','response_code','response_text','message_id','exported']
    list_filter = ['frequency','currency','test']
    date_hierarchy = 'next_payment_date'

class DonationAdmin(admin.ModelAdmin):
    list_display = ('message_id','donor','currency','amount','test','exported','commitment')
    list_filter = ['currency','test']
    date_hierarchy = 'date_time'
    readonly_fields = ['commitment','donor','amount','currency','cc_expiry','test','cc_last_four','transaction_id','client_id','settlement_date','xml_request','xml_response',
                       'request_type','response_code','action_type','purchase_order','response_code','iteration','response_text','message_id','exported']
    fieldsets = (('Contact Info', {'fields': ('title','f_name','l_name','address','address2','city','state','postcode','country','phone','email')}),
                ('Donation Info', {'fields': ('amount','currency','iteration','project_code','project_name')}),
                ('Transaction Info', {'classes': ('collapse',), 'fields': ('response_code','response_text','xml_request','xml_response')})
                )

admin.site.register(Project, ProjectAdmin)
admin.site.register(Donor, DonorAdmin)
admin.site.register(Commitment, CommitmentAdmin)
admin.site.register(Donation, DonationAdmin)