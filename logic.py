"""
This file contains the business logic for the donate app
"""
from donate.models import Donation, Donor, Project, Commitment, DonorForm, CommitmentForm
from donate.gateway import GatewayConnector
from django.conf import settings
import django
import ConfigParser


def saveDonor(formdata):
    # Load config file
    config = ConfigParser.RawConfigParser()
    config.read(settings.SITE_ROOT+'/donate/config/config.ini')
    
    # set initial values
    formdata['test'] = config.get('basic','test')
    formdata['created_date'] = django.utils.timezone.now()

    # create the object with the data submitted.
    donor = DonorForm(formdata)

    # save the donor model object in a way that you get the id back.
    donorModelObject = donor.save(False)
    donorModelObject.save()
    return donorModelObject


def processTransaction(type, formdata, client_id):
    # create the gateway connector object
    sc = GatewayConnector()
    # set the values from the form
    sc.transaction = {'amount':formdata['amount'],
                      'currency':formdata['currency'],
                      'cardnumber':formdata['cc_number'], 
                      'cvv':formdata['cc_cvv'],
                      'client_id': client_id, 
                      'expiry':formdata['cc_expiry'].strftime('%m/%y')}
    # create and send an XML request to the gateway
    sc.generate_xml_request(type)
    sc.send()

    return sc.response_data, sc.xml_request, sc.xml_response

def save(type, grd, data, donor, client_id, xml_request, xml_response):
        # read the config from the config file 
        config = ConfigParser.RawConfigParser()
        config.read(settings.SITE_ROOT+'/donate/config/config.ini')

        # modify the data somewhat to make it suitable for a database
        data.update ({
                     'client_id': client_id,
                     'type': type,
                     'result': grd.responsetext.text[0],
                     'test': config.get('basic','test'),
                     'cc_last_four': data['cc_number'],
                     'cc_expiry': data['cc_expiry'],
                     'project_code': data['project'].code,
                     'project_name': data['project'].name,
                     'request_type':grd.requesttype.text,
                     'response_code':grd.responsecode.text,
                     'response_text':grd.responsetext.text,
                     'message_id':grd.messageid.text,
                     'xml_request': xml_request,
                     'xml_response': xml_response,
                     })
        if type == 'triggered':
            iterations_name = data['repeat'][:-2]+'s'
            data.update({'action_type':grd.actiontype.text,
                        'frequency': data['repeat'],
                        'next_payment_date': data['start_date'],
                        'iterations_total': data[iterations_name],
                        'periodic_type': 4,
                        'donor':donor.id,
                        })
            commitment = CommitmentForm(data)
            commitmentObject = commitment.save(False)
            commitmentObject.save()
            return commitmentObject.id, commitmentObject.message_id
        else:
            data['settlement_date']= grd.settlementdate.text 
            data['transaction_id']= grd.txnid.text
            donation = Donation()
            donation.donor = donor
            donation.merge(data)
            donation.save()  
            return donation.id, donation.message_id

        send_mail('Thank you for your donation',render_to_string('donate/emails/thanks_regular.txt',{'commitment':commitment, 'donor':donor}),config.get('basic','from_email'),[donor.email])
