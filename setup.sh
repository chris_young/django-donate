#!/bin/bash
# setup.sh - useful script for getting a virtualenv etc all set up.
#
####################################################
# Config:

DJANGODONATEDIR="$(dirname $0)"
APPNAME="$(basename "$DJANGODONATEDIR")"

if [[ "$DJANGODONATEDIR" == '.' ]]; then
    echo 'Please do NOT run this script from within the django-donate directory!'
    echo 'This is packaged as a django APP, so should be run from the parent directory,'
    echo 'where I will set up the django environment for you, unless you are installing'
    echo 'into an already built django instance, in which case you will have to do it'
    echo 'yourself.'
    exit 1
fi

if [[ "$DJANGODONATEDIR" != './donate' ]]; then
    if [[ -d 'donate' ]]; then
        echo "$DJANGODONATEDIR"
        echo "Sorry - the folder this app is in is NOT called 'donate' (but it needs to be)"
        echo "   I would rename it, but you already have a donate folder! I don't know what"
        echo "   do do!"
        exit 2
    fi
    echo "renaming the app module to 'donate'."
    mv "$DJANGODONATEDIR" "./donate"
    DJANGODONATEDIR="./donate"
fi

VDIR=.virtualenv
PYTHON=$VDIR/bin/python
PIP=$VDIR/bin/pip

# You shouldn't need to change these...
VIRTUALENV_VERSION=1.9.1

VIRTUALENV="$VDIR/virtualenv-$VIRTUALENV_VERSION/virtualenv.py"

####################################################
# Actually start doing stuff:

# with clean command, remove old virtualenv
if [[ "$1" == "clean" ]]; then
    echo "Removing Old VirtualEnv"
    rm -rf "$VDIR"
    # if "./setup.sh clean only", quit after cleaning.
    [[ "$2" == "only" ]] && exit 0
fi

# if the .virtualenv folder doesn't exist, make it
if [[ ! -d "$VDIR" ]]; then
    echo "Making new folder ($VDIR) for virtualenv to live in."
    mkdir "$VDIR"
fi

# extract virtualenv.py if we need it
if [[ ! -f "$VIRTUALENV" ]]; then
    PACKAGE="virtualenv-1.9.1.tar.gz"
    echo "Downloading VirtualEnv ($VIRTUALENV_VERSION)"
    curl "https://pypi.python.org/packages/source/v/virtualenv/$PACKAGE" -o "$VDIR/$PACKAGE"
    if [[ $? -ne 0 ]]; then
        echo 'Download Failed!'
        exit 1
    fi
    echo "Extracting VirtualEnv..."
    tar -zxC "$VDIR" -f "$VDIR/$PACKAGE"
fi

# if there's no python in virtualenv, make one:
[[ -f "$PYTHON" ]] || python "$VIRTUALENV" "$VDIR"

# get required python modules:
echo "Checking requirements (python modules)"
$PIP install -r "$DJANGODONATEDIR/requirements.txt"
if [[ $? -ne 0 ]]; then
    echo 'Something went wrong trying to install the required python modules...'
    exit 2
fi

if [[ ! -e "manage.py" ]]; then
    echo "You don't seem to be already running django here, so I'll copy in the default settings..."
    ln -s "$DJANGODONATEDIR/.django-project/manage.py"
    chmod +x manage.py

    if [[ ! -d "donateproject" ]]; then
        echo "I'll link in the basic django Project for this app (django-donate) to live in."
        ln -s "$DJANGODONATEDIR/.django-project/donateproject"
    fi

    if [[ ! -d "static" ]]; then
        echo "copying in the static project files"
        ln -s "$DJANGODONATEDIR/.django-project/static"
    fi
    if [[ ! -d "templates" ]]; then
        echo "copying in the templates project files"
        ln -s "$DJANGODONATEDIR/.django-project/templates"
    fi
    echo 'Running syncdb'
    ./manage.py syncdb

fi

echo 'Done!'
